---
layout: base.njk
title: A/B Testing
date: 2020-08-25
eleventyNavigation:
  key: A/B Testing
  parent: Application Deployment Scenarios
  order: 4
---

## **9.1.2** Advance Deployment

**A/B Testing**

A/B testing is a release strategy. It's focused on experimenting with features.
<br>

![><](/content/images/abtesting.png)

<br>

**How it works**

With A/B testing, implementation is similar to the process of canary releasing. You have a baseline feature, the "A" feature, and then you deploy or release a "B" feature. You then use monitoring or instrumentation to observe the results of the feature release. Hopefully, this will reveal whether or not you achieved what you wanted.

You're not only limited to two versions in a test. Netflix, for example, has multiple covers it displays as the graphic for the same show based on the version it predicts users will respond to and want to see. But be careful: It's healthy to only run one experiment at a time.


**What's It Good For**

A/B testing from the 1,000-foot view may look a lot like canary releasing: You have a set of users seeing the new stuff, and a set with the old stuff. However, A/B has a much different intent. While the focus of canary releasing is risk mitigation, A/B testing's focus is on how useful a feature is for customers. It's the old argument of "build the thing right" vs. "build the right thing."


