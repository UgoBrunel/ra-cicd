---
layout: base.njk
title: Deployments 
date: 2020-08-25
eleventyNavigation:
  key: Deployments
  parent: Detail Phases Explanation
  order: 2
---

## **9.1.1** Deployments
<br>

**Blue/Green Deployment** 

Blue/green deploys are focused purely on deployment as a way of eliminating downtime and disruption for customers.

![><](/content/images/bluegreen.png)

<br>

**How it works**

Utilization of a Blue/Green Deployment process reduces risk and down time by creating a mirror copy your production environment naming one Blue and one Green. Only one of the environments is live at any given time serving live production traffic. During a deployment software is deployed to the non-live environment – meaning live production traffic is unaffected during the process. Tests are run against this currently non-live environment and once all tests have satisfied the predefined criteria traffic routing is switched to the non-live environment making it live. The process is repeated in the next deployment with the original live environment now becoming non-live.

<br>

**What's It Good For**

Blue/green deployment drastically reduces risk in many situations. If you have a site where it costs significantly to be down, even for a few minutes, this option can save your bacon.
