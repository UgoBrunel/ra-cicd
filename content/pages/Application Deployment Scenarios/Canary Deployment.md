---
layout: base.njk
title: Canary Deployment 
date: 2020-08-25
eleventyNavigation:
  key: Canary Deployment
  parent: Application Deployment Scenarios
  order: 3
---

## **9.1.2** Advance Deployment

**Canary Deployment**

Canary deployments, also known as canary releasing, are usually release-focused. But, sometimes, they can also be deployment-focused. They are deployment-focused when you use your deployment scripts to only update the code on specific containers or servers. They're release-focused when you can change which canary features are visible to some users without redeploying.

<br>

![><](/content/images/advancedeployments.png)

<br>

**How it works**

Differently to Blue/Green deployments, Canary Deployments do not rely on duplicate environments to be running in parallel. Canary Deployments roll out a release to a specific number or percentage of users/servers to allow for live production testing before continuing to roll out the release across all users/servers. The prime benefit of canary releases is the ability to detect failures early and roll back changes limiting the number of affected users/services in the event of exceptions and failures.

**What's It Good For**

Canary is fantastic for lowering the risk of changes to production. The "no defects in production" mentality is a little overrated, and canary lets you mitigate the cost of defects without spending an enormous amount on preventive testing. (You should absolutely spend some effort on preventive testing, though.)


