---
layout: base.njk
title: Application Deployment Scenarios
date: 2020-08-25
eleventyNavigation:
  key: Application Deployment Scenarios
  parent: CI/CD Architecture Framework
  order: 9
---
# **9.** Application Deployment Scenarios

Software deployment includes all the process required for preparing a software application to run and operate in a specific environment. It involves installation, configuration, testing and making changes to optimize the performance of the software. It can either be carried out manually or through automated systems has shown in Fujitsu Framework. 

With this in mind Fujitsu acknowledge three main ways of deploying separated into main sections Deployment and Advance Deployments.

<br>

- Deployments
  -  [Blue/Green Deployment](/content/pages/Application%20Deployment%20Scenarios/Deployments.md)

- Advance Deployments
  - [Canary Deployment](/content/pages/Application%20Deployment%20Scenarios/Advance%20Deployments.md)
  - [A/B Deployment](/content/pages/Application%20Deployment%20Scenarios/ab_testing.md)
