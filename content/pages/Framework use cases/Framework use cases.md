---
layout: base.njk
title: Framework use cases  
date: 2020-08-25
eleventyNavigation:
  key: Framework use cases  
  parent: CI/CD Architecture Framework
  order: 12
---
# **13.Framework use cases**

In this bellow scenario is possible to see a real live use case how to **deploy a Kubernetes Cluster** choosing the correct tools to this scenario and matching with the different phases of the Fujitsu Framework.

**Note:** This is illustrative example of a use-case and tool combination. Customer solution may look different depending on footprint, use case, customer preferences and requirements and shall be evaluate case by case


<br>

![><](/content/images/realusecase.png)

<br>
