---
layout: base.njk
title: Fujitsu Framework Workflow phases
date: 2020-08-25
eleventyNavigation:
  key: Fujitsu Framework Workflow phases
  parent: CI/CD Architecture Framework
  order: 7
---
# **7.**	Fujitsu Framework Workflow phases
<br>![](/content/images/workflow.png)

<br>For a better understanding of the above macro blocks and their functionality the following diagram will present how each macro block relates with the Fujitsu framework phases.

<br>![](/content/images/phasesoverview.png)