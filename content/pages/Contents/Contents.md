---
layout: base.njk
title: Contents
date: 2020-08-25
eleventyNavigation:
  key: Contents
  parent: CI/CD Architecture Framework
  order: 1
---
# **Contents**

## **1.** Introduction and Overview
## **2.**	Customer Challenges	
## **3.**	The Why, What and How of the Fujitsu Framework	
- **3.1.1**	Why: The DevOps values proposition	
- **3.1.2**	What: Yet another definition of DevOps	
- **3.1.3**	How: A DevOps checklist	
- **3.1.4**	Customer-Oriented Culture	
- **3.1.5**	Source Control & Revisions	
- **3.1.6**	Infrastructure-As-Code	
- **3.1.7**	Continuous Integration, Continuous Delivery and Continuous Deployment	
## **4.**	Fujitsu Value Proposition	
## **5.**	Framework explanation	
## **6.**	Main Building Blocks explain	
- **6.1.1**	Pipeline	
- **6.1.2**	Continuous Integration (CI)	
- **6.1.3**	Continuous Delivery (CD)	
- **6.1.4**	Continuous Deployment (CD)	
- **6.1.5**	Observability and Analysis	
- **6.1.6**	Collaboration and Documentation	
## **7.**	Fujitsu Framework Workflow phases	
## **8.**	Detail Phases Explanation	
## **9.**	Application Deployment Scenarios	
- **9.1.1**	Deployments	
- **9.1.2**	Advance Deployment	
## **10.**	Object deliverable concepts	
- **10.1.1**	Monolithic	
- **10.1.2**	Containerized	
- **10.1.3**	Serverless	
- **10.1.4**	APIs	
- **10.1.5**	DevOps as Code	
## **11.**	Use cases deep dive	
- **11.1.1**	Deploy an infrastructure	
- **11.1.2**	Deploy an Application	
**12.**	Tooling	
**13.**	Framework use cases	
**14.**	Conclusion	
