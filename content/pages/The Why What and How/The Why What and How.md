---
layout: base.njk
title: The Why What and How of the Fujitsu Framework
date: 2020-08-25
eleventyNavigation:
  key: The Why What and How 
  parent: CI/CD Architecture Framework
  order: 2
---


## **3.**	The Why, What and How of the Fujitsu Framework	
<br>

**3.1.1**   [Why: The DevOps values proposition](/pages/TheWhyWhatandHow/TheWhyWhatandHow.md)

**3.1.2**   [What: Yet another definition of DevOps](http://fujitsu-cloud-services.pages.wadatsumi.dat.css.fujitsu.com/docs-as-code/ra-cicd/master/content/pages/The%20Why%20What%20and%20How/What//index.html)

**3.1.3**   [How: A DevOps checklist](http://fujitsu-cloud-services.pages.wadatsumi.dat.css.fujitsu.com/docs-as-code/ra-cicd/master/content/pages/The%20Why%20What%20and%20How/How%20A%20DevOps%20checklist//index.html)	

**3.1.4**   [Customer-Oriented Culture](http://fujitsu-cloud-services.pages.wadatsumi.dat.css.fujitsu.com/docs-as-code/ra-cicd/master/content/pages/The%20Why%20What%20and%20How/Customer-Oriented%20Culture//index.html)	

**3.1.5**   [Source Control & Revisions](http://fujitsu-cloud-services.pages.wadatsumi.dat.css.fujitsu.com/docs-as-code/ra-cicd/master/content/pages/The%20Why%20What%20and%20How/Source%20Control%20&%20Revisions//index.html)	

**3.1.6**   [Infrastructure-As-Code](http://fujitsu-cloud-services.pages.wadatsumi.dat.css.fujitsu.com/docs-as-code/ra-cicd/master/content/pages/The%20Why%20What%20and%20How/Infrastructure-As-Code//index.html)	

**3.1.7**   [Continuous Integration, Continuous Delivery and Continuous Deployment](http://fujitsu-cloud-services.pages.wadatsumi.dat.css.fujitsu.com/docs-as-code/ra-cicd/master/content/pages/The%20Why%20What%20and%20How/Continuous%20Integration,%20Continuous%20Delivery%20and%20Continuous%20Deployment//index.html)	


