---
layout: base.njk
title: How A DevOps checklist
date: 2020-08-25
eleventyNavigation:
  key: How A DevOps checklist
  parent: The Why What and How
  order: 3
---
# **3.1.3** How: A DevOps checklist

This checklist is neither unique nor static. This will help setting up a DevOps culture and toll suite, but don’t consider it as a unique way to proceed with your organization transformation.
