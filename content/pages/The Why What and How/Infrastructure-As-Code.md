---
layout: base.njk
title: Infrastructure-As-Code
date: 2020-08-25
eleventyNavigation:
  key: Infrastructure-As-Code
  parent: The Why What and How
  order: 6
---

# **3.1.6**	Infrastructure-As-Code

The fact of enabling the Continuous Configuration Automation approach is becoming a key step in the life cycle of a product.
