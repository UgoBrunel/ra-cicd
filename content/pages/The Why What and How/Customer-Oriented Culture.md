---
layout: base.njk
title: Customer-Oriented Culture
date: 2020-08-25
eleventyNavigation:
  key: Customer-Oriented Culture
  parent: The Why What and How 
  order: 4
---
#	**3.1.4** Customer-Oriented Culture

No one knows how a product should work better than the customer himself. A customer-oriented culture does not focus on building the best product but rather focuses on creating the best solution for your customer. It is also important to stop worrying about creating new products or features and instead focusing on new customer needs to fill.


