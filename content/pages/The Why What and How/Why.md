---
layout: base.njk
title: Why The DevOps values proposition
date: 2020-08-25
eleventyNavigation:
  key: Why The DevOps values proposition
  parent: The Why What and How 
  order: 1
---
#	**3.1.1** Why: The DevOps values proposition

Because the thing that we need to value most is how our products and services are making impact to business. It can be deploying feature that create new profit, fixing bug, increasing deployment time or anything.
From ideation, architecting and designing, setting up infrastructure, coding, to releasing in production. And after all that, of course we need to care about learning through monitoring and improving performance if needed. It is a lot and it is not just about writing code.

