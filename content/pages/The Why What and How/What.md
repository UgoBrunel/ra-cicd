---
layout: base.njk
title: What Yet another definition of DevOps
date: 2020-08-25
eleventyNavigation:
  key: What Yet another definition of DevOps
  parent: The Why What and How
  order: 2
---
# **3.1.2** What: Yet another definition of DevOps

Simply put DevOps enables development teams and operation teams to work better together. DevOps also aims to reduce the time taken by teams to reflect changes in production.
The term DevOps emerged when small startups started disrupting different markets with very small teams, maybe 4 developers, who were creating new products and at the same time covering the work usually done by big larger of IT system operations people. They felt the need of applying the same practices and processes of coding into operations – scripting, automation, version control, etc.
DevOps has since evolved into a cultural movement or practice that emphasizes the collaboration and communication of both developers and other IT professionals.

