---
layout: base.njk
title: Continuous Integration, Continuous Delivery and Continuous Deployment
date: 2020-08-25
eleventyNavigation:
  key: Continuous Integration, Continuous Delivery and Continuous Deployment
  parent: The Why What and How 
  order: 7
---
# **3.1.7**	Continuous Integration, Continuous Delivery and Continuous Deployment

Continuous integration is an important brick in the DevOps settlement and the weak link in the automation process since it is positioned between development and operations in order to automate the flow and fluidity the passage of an application from development to post-development operations.
Continuous delivery is a practice designed to ensure that code can be rapidly and safely deployed to production and ensuring business applications and services function as expected through rigorous automated testing.
Continuous deployment is the next step of continuous delivery. Every release that passes the automated tests is deployed to production automatically. Continuous deployment should be the goal of most companies that aim to have an end to end lifecycle for their products.
