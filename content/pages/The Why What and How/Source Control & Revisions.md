---
layout: base.njk
title: Source Control & Revisions
date: 2020-08-25
eleventyNavigation:
  key: Source Control & Revisions
  parent: The Why What and How
  order: 5
---
# **3.1.5** Source Control & Revisions

Version control is not just for developers and it operations, since one of the best emerging practices in startups and enterprises is versioning documentation. Documentation versioning will help you track incremental backups, record any change and revert documentation to an earlier version and Track co-authoring and collaboration and individual contributions.
