---
layout: base.njk
title: 14.	Conclusion
date: 2020-08-25
eleventyNavigation:
  key: Conclusion
  parent: CI/CD Architecture Framework
  order: 14
---
# **14. Conclusion** - TESTEAHHHH

Software is critical to all organizations, but producing great applications and services that meet business needs requires modern development and delivery processes. This is especially important in today's digital economy, where strategic software development fuels innovation and boosts competitive advantage.

With the shift to digital initiatives, the line between development and IT on one side and the business on the other is blurring. It's increasingly difficult to unravel the dependencies.
As companies adapt to compete in today’s marketplace, business leaders need to be aggressive and intentional about driving adoption of agile and DevOps within their organizations.
Agile and DevOps go hand in hand. Agile Development focuses on business agility by Dev Team collaborating with Business and creating potentially shippable increments of the Product. DevOps is aimed at collaboration between the Development Team and the Operations team to produce working production increments.

The DevOps framework helps improve an organization's time-to-market significantly by bringing together the twin functions of development and operations in the software application space. However, by implementing DevOps, organizations must incur radical changes to their technology, process, and support culture.

Software is critical to all organizations, but producing great applications and services that meet business needs requires modern development and delivery processes. This is especially important in today's digital economy, where strategic software development fuels innovation and boosts competitive advantage.

With the shift to digital initiatives, the line between development and IT on one side and the business on the other is blurring. It's increasingly difficult to unravel the dependencies.
As companies adapt to compete in today’s marketplace, business leaders need to be aggressive and intentional about driving adoption of agile and DevOps within their organizations.