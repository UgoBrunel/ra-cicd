---
layout: base.njk
title: Object deliverable concepts
date: 2020-08-25
eleventyNavigation:
  key: Object deliverable concepts
  parent: CI/CD Architecture Framework
  order: 10
---
# **10.Object deliverable concepts**

Modern applications are often distributed and cloud-based. They can elastically scale to meet the needed demand, and are more resilient to failure thanks to highly-available architectures. 

In this section there some of the more common deliverables that you can deploy using the Fujitsu framework divided by groups.

<br>

![><](/content/images/frameworkd.png)

<br>

**10.1.1	Monolithic**

A monolithic application is self-contained, and independent from other computing applications. The design philosophy is that the application is responsible not just for a particular task, but can perform every step needed to complete a particular function. Today, some personal finance applications are monolithic in the sense that they help the user carry out a complete task, end to end, and are private data silos rather than parts of a larger system of applications that work together. 

<br>

**10.1.2	Containerized**

 - **MicroServices**

    - Microservices are a cloud native architectural approach in which a single application is composed of many loosely coupled and independently deployable smaller components, or services.

<br>

 - **ServiceMesh**

    - A service mesh is a configurable, low-latency infrastructure layer designed to handle a high volume of network-based interprocess communication among application infrastructure services using application programming interfaces (APIs). A service mesh ensures that communication among containerized and often ephemeral application infrastructure services is fast, reliable, and secure. The mesh provides critical capabilities including service discovery, load balancing, encryption, observability, traceability, authentication and authorization, and support for the circuit breaker pattern.

<br>

**10.1.3	Serverless**

Serverless is the native architecture of the cloud that allows you to build and run applications and services without thinking about servers. It eliminates infrastructure management tasks such as server or cluster provisioning, patching, operating system maintenance, and capacity provisioning. You can build them for nearly any type of application or backend service, and everything required to run and scale your application with high availability is handled for you.

<br>

**10.1.4	APIs**

An API is defined as a specification of possible interactions with a software component. What does that mean, exactly? Well, imagine that a car was a software component. Its API would include information about what it can do—accelerate, brake, turn on the radio, etc. It would also include information about how you could make it do those things. For instance, to accelerate, you put your foot on the gas pedal and push.

<br>

**10.1.5	DevOps as Code**

- Infrastructure as Code (IAC) 

  Infrastructure as Code (IaC) is the management of infrastructure (networks, virtual machines, load balancers, and connection topology) in a descriptive model, using the same versioning as DevOps team uses for source code. Like the principle that the same source code generates the same binary, an IaC model generates the same environment every time it is applied.

  Programmatic configuration captures the idea that we can define our infrastructure in a machine readable format. For example, you describe a planned cluster build in YAML to execution by a tool such as Terraform that requests the resources from your dynamic infrastructure and, hopefully, tracks that resource descriptors you get back from that request. 

<br>

- Pipeline as Code (PaC)

  Pipelines as code allow you to write your deployment pipeline with code which means you can easily collaborate on it without having to make manual edits inside of the tool. When you update your pipeline, the CI/CD tool will automatically pick up the pipeline changes without any manual intervention

<br>

- Platform as code
  
  Platform as code (PaC) is a similar abstraction of the platform layer. PaC allows you to write declarative instructions about the platform layer — including the OS and other tools that are needed for development and operations of your application — into code and execute it.

