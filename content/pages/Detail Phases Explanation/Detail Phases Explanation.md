---
layout: base.njk
title: Detail Phases Explanation
date: 2020-08-25
eleventyNavigation:
  key: Detail Phases Explanation
  parent: CI/CD Architecture Framework
  order: 8
---
# **8.**	Detail Phases Explanation

### **Code Phase**
<br>

![><](/content/images/codephase.png)

The code phase starts after the code commit

In the code phase all development should be close and all static-code analysis, configurations and code review must be done. 

This phase ends with a code pull to the repository

<br>

### **Build Phase**
<br>

![><](/content/images/buildphase.png)

The Build phase is where DevOps really kicks in. 

Once a developer has finished a task, they commit their code to a shared code repository. There are many ways this can be done, but typically the developer submits a pull request — a request to merge their new code with the shared codebase.

A pull request triggers an automated process which builds the codebase and runs a series of end-to-end, integration and unit tests to identify any regressions.

<br>

### **Test Phase**

<br>

![><](/content/images/testphase.png)

Once a build succeeds, the Test phase starts. This phase is where all the requirements testing are done like the integration test with other components, load & stress testing, UI testing between other necessary tests.

At the same time, automated tests might run security scanning against the application, check for changes to the infrastructure and compliance with hardening best-practices, test the performance of the application or run load testing. The testing that is performed during this phase is up to the organization and what is relevant to the application, but this stage can be considered a test-bed that lets you plug in new testing without interrupting the flow of developers or impacting the production environment.

<br>

### **Release Phase**
<br>

![><](/content/images/releasephase.png)

The Release phase is a milestone in a DevOps pipeline — it’s the point at which we say a build is ready for deployment into the production environment. By this stage, each code change has passed a series of manual and automated tests, and the operations team can be confident that breaking issues and regressions are unlikely.

Depending on the DevOps maturity of an organization, they may choose to automatically deploy any build that makes it to this stage of the pipeline.
Alternatively, an organization may want to have control over when builds are released to production.

<br>

### **Deploy Phase**
<br>

![><](/content/images/deployphase.png)

Finally, a build is ready for the big time and it is released into production. There are several tools and processes that can automate the release process to make releases reliable with no outage window.

The same Infrastructure-as-Code that built the test environment can be configured to build the production environment. We already know that the test environment was built successfully, so we can rest assured that the production release will go off without a hitch.

For example a blue-green deployment (explain bellow) lets us switch to the new production environment with no outage. Then the new environment is built, it sits alongside the existing production environment. When the new environment is ready, the hosting service points all new requests to the new environment. If at any point, an issue is found with the new build, you can simply tell the hosting service to point requests back to the old environment while you come up with a fix.

<br>

### **Run/Operate Phase**
<br>

![><](/content/images/runoperatephase.png)

**The new release is now live and being used by the customers. Great work!**

The operations team is now hard at work, making sure that everything is running smoothly. Based on the configuration of the hosting service, the environment automatically scales with load to handle peaks and troughs in the number of active users.

The "final" phase of the DevOps cycle is to monitor the environment. This builds on the customer feedback provided in the Operate phase by collecting data and providing analytics on customer behavior, performance, errors and more.

<br>

![><](/content/images/fingers.png)
