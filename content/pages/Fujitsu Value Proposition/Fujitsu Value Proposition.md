---
layout: base.njk
title: Fujitsu Value Proposition
date: 2020-08-25
eleventyNavigation:
  key: Fujitsu Value Proposition
  parent: CI/CD Architecture Framework 
  order: 3
---
# **4.**	Fujitsu Value Proposition

- Fully managed framework, agnostic from cloud provider and environment (public, private and hybrid);

- Using leading solutions (Open Source / enterprise tools) on the market;

- Agnostic to support multiple cloud providers, and cloud-native deliverables (K8s, kubernetes, service mesh, microservices, …); 

- Customizable to meet customer requirements, maturity, existing footprint and budget constraints; 

- It’s available through Fujitsu Software Factory proposing everything as code, and aligned with CN-SDP NWE framework.

![](/content/images/productivity.png)