---
layout: base.njk
title: Continuous Deployment (CD)
date: 2020-08-25
eleventyNavigation:
  key: Continuous Deployment (CD)
  parent: Main Building Blocks explain
  order: 4
---

# **6.1.4** Continuous Deployment (CD)

Continuous deployment goes one step further than continuous delivery. With this practice, every change that passes all stages of your production pipeline is released to your customers. There's no human intervention, and only a failed test will prevent a new change to be deployed to production.
Continuous deployment is an excellent way to accelerate the feedback loop with your customers and take pressure off the team as there isn't a Release Day anymore. Developers can focus on building software, and they see their work go live minutes after they've finished working on it.



