---
layout: base.njk
title: Collaboration and Documentation 
date: 2020-08-25
eleventyNavigation:
  key: Collaboration and Documentation
  parent: Main Building Blocks explain
  order: 6
---

# **6.1.6** Collaboration and Documentation
Increased communication and collaboration in an organization is one of the key cultural aspects of DevOps. The use of DevOps tooling and automation of the software delivery process establishes collaboration by physically bringing together the workflows and responsibilities of development and operations. Building on top of that, these teams set strong cultural norms around information sharing and facilitating communication through the use of chat applications, issue or project tracking systems, and wikis. This helps speed up communication across developers, operations, and even other teams like marketing or sales, allowing all parts of the organization to align more closely on goals and projects.