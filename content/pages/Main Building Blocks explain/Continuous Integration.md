---
layout: base.njk
title: Continuous Integration
date: 2020-08-25
eleventyNavigation:
  key: Continuous Integration
  parent: Main Building Blocks explain
  order: 2
---

# **6.1.2**	Continuous Integration (CI)

Is a development practice that requires developers to integrate code into a shared repository.
Developers practicing continuous integration merge their changes back to the main branch as often as possible. The developer's changes are validated by creating a build and running automated tests against the build. By doing so, you avoid the integration hell that usually happens when people wait for release day to merge their changes into the release branch.
Continuous integration puts a great emphasis on testing automation to check that the application is not broken whenever new commits are integrated into the main branch.

