---
layout: base.njk
title: Main Building Blocks explain
date: 2020-08-25
eleventyNavigation:
  key: Main Building Blocks explain
  parent: CI/CD Architecture Framework 
  order: 3
---
# **6.**	Main Building Blocks explain

To apply the DevOps methodology Fujitsu divided the framework into four main blocks, thus following a modular vision.

<br>![](/content/images/mainblock.png)

<br>
In this way these same blocks can be worked together or individually, thus covering a greater number of requirements, managing to cover a greater number of customers.

<br>

- [**6.1.1**	Pipeline](/content/pages/Main%20Building%20Blocks%20explain/Pipeline.md)

- [**6.1.2**	Continuous Integration (CI)]()

 - [**6.1.3**	Continuous Delivery (CD)]()

6.1.4	Continuous Deployment (CD)

6.1.5	Observability and Analysis

6.1.6	Collaboration and Documentation