---
layout: base.njk
title: Pipeline
date: 2020-08-25
eleventyNavigation:
  key: Pipeline
  parent: Main Building Blocks explain
  order: 1
---

# **6.1.1**	Pipeline

CI/CD pipeline is the glue that pulls all these pieces together to create a resilient environment by continuously adjusting the system components based on the operators IaC descriptions. The pipeline relies on having dynamic infrastructure managed to desired state as a target; however, it adds the critical concepts of staged changes over time to the system. Since we need to create systems that can adapt and improve over time, the CI/CD pipeline provides mechanism for controlled implementation change over time.

<br>![](/content/images/pipeline.png)
