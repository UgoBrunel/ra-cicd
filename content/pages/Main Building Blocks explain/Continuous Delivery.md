---
layout: base.njk
title: Continuous Delivery 
date: 2020-08-25
eleventyNavigation:
  key: Continuous Delivery
  parent: Main Building Blocks explain
  order: 3
---

# **6.1.3** Continuous Delivery (CD)

Is the natural extension of Continuous Integration.

This means that on top of having automated your testing, you also have automated your release process and you can deploy your application at any point of time by clicking on a button.
In theory, with continuous delivery, you can decide to release daily, weekly, fortnightly, or whatever suits your business requirements. 


