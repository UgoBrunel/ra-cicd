---
layout: base.njk
title: Use cases deep dive 
date: 2020-08-25
eleventyNavigation:
  key: Use cases deep dive
  parent: CI/CD Architecture Framework
  order: 2
---

# **11.	Use cases deep dive**

In this section lets deep dive in both use cases:

 [- Deploy an infrastructure;](/content/pages/Use%20cases%20deep%20dive/Deploy%20an%20infrastructure.md)

 [- Deploy an Application;](/)
