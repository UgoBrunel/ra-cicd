---
layout: base.njk
title: Deploy an infrastructure 
date: 2020-08-25
eleventyNavigation:
  key: Deploy an infrastructure 
  parent: Use cases deep dive
  order: 2
---
# **11.1.1	Deploy an infrastructure** 

<br>

![><](/content/images/infradeploy.png)

<br>

**1.**	Commit  infrastructure code to the repository (Pull Code);

**2.**	Pipeline is trigger with the previous pull code;

**3.**	The Pipeline will do two different process inside the CI(Continuous Integration) and the CD (Continuous Delivery);

**4.** CI will test, validate and create our infra code and build an image;

**5.** After the CD parts get trigger by the CI our artifact is check in all security and compliance measures and if all of this was successful the build is now save in the infra is ready to be deploy;

**6.**	If necessary the config management will provisioning some templating to our environment and automate some tasks like installation of  Nginx or Apache for example;

**7.**	This deploy is done automatically;

**8.**	Now that the build is ready our code id finally deploy to our cloud provider;
