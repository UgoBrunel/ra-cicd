---
layout: base.njk
title: Deploy an Application 
date: 2020-08-25
eleventyNavigation:
  key: Deploy an Application 
  parent: Use cases deep dive
  order: 3
---
# **11.1.2	Deploy an Application** 

<br>

![><](/content/images/infradeploy.png)

<br>

**1.**	Commit  infrastructure code to the repository (Pull Code);

**2.**	Pipeline is trigger with the previous pull code;

**3.**	The Pipeline will do two different process inside the CI(Continuous Integration) and the CD (Continuous Delivery);

**4.**	CI will test, validate and guarantee the quality of our code and build our artifact;

**5.**	Now it's the time to create our app images and added to our image repository;

**6.**	After the previous state we run some load/performance and other test to our code and the artifact is created. If the testing phase goes successfully the  CD part of our pipeline is trigger;

**7.**	In this phase our image is testing in security measures and all the credentials maintain secure in a vault;

**8.**	The configure management will automate some specific task or even install some specific tools required via templating;

**9.**	After all this our orchestrator is build and connect to our images;

**10.**	This type our deploy can be done manually or automatically using continuous deployment depending on customer requirements our maturity;

**11.**	Congratulations our app or apps are now deploy successfully this workflow with the exception of the build phase should now be redo to the different phases deployment phases(QA-Staging-Prod);

