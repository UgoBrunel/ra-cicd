---
layout: base.njk
title: Tooling
date: 2020-08-25
eleventyNavigation:
  key: Tooling
  parent: CI/CD Architecture Framework
  order: 12
---
# **12. Tooling**

Fujitsu select tools that can help you understand the productivity of your DevOps processes, both automated and manual, and to determine if they are working in your favor. 

Fujitsu do two things with these tools. First, define which metrics are relevant to the DevOps processes, such as speed to deployment versus testing errors found. Second, define automated processes to correct issues without human involvement. An example would be dealing with software scaling problems automatically on cloud-based platforms. 

Neither to say there are a lot of great tools like the ones that we can see in the image bellow.


<br>

![><](/content/images/tooling.png)

<br>


With all previous thinks in mind and with the mindset that a tool or even one framework don’t fit all our customers case Fujitsu had developed their own Framework that will help the migration to the Cloud environments with their own set of tools.

In the image below is all the tools that Fujitsu have in their Framework.

We that tools Fujitsu customers can be sure that all the steps and stages in a Migration to a DevOps methodologies are cover by this Framework and Fujitsu have to competence help the customers move even further and faster.

<br>

![><](/content/images/toolchain.png)
