//const markdownlink = require("markdown-it-replace-link");
const { DateTime } = require("luxon");
const eleventyNavigationPlugin = require("@11ty/eleventy-navigation");
const readingTime = require("eleventy-plugin-reading-time");
const eleventyPluginSyntaxHighlighter = require('@11ty/eleventy-plugin-syntaxhighlight');

module.exports = function(eleventyConfig) {

    // add navigation menu plug in
    eleventyConfig.addPlugin(eleventyNavigationPlugin);
  
    // add reading time plugin 
    eleventyConfig.addPlugin(readingTime);

    // Date formatting (human readable)
    eleventyConfig.addFilter("readableDate", dateObj => {
        return DateTime.fromJSDate(dateObj).toFormat("dd LLL yyyy");
    });


    // Mermaid to PNG
    eleventyConfig.addPlugin(eleventyPluginSyntaxHighlighter);
    const highlighter = eleventyConfig.markdownHighlighter;
    eleventyConfig.addMarkdownHighlighter((str, language) => {
        if (language === "mermaid") {
            return `<pre class="mermaid">${str}</pre>`;
        }
        return highlighter(str, language);
    });
    
    eleventyConfig.setTemplateFormats([
        "md",
        "png",
        "css",
        "ico",
        "jpg"
    ]);

    /* Markdown Plugins */
    let markdownIt = require("markdown-it");
    let markdownItAnchor = require("markdown-it-anchor");
    //let markdownlink = require("markdown-it-replace-link");
    let options = {
        html: true,
        breaks: true,
        linkify: true
    };
    let opts = {
        permalink: false
    };

    eleventyConfig.setLibrary("md", markdownIt(options)
        .use(markdownItAnchor, opts)
        
    );

    // set prefix for URLs
    let prefix = process.env.ELEVENTY_ENV;
    
    if (typeof prefix == 'undefined')
        prefix = "";
    prefix = prefix.replace(/ /g,"");
    console.log("Site Prefix:'"+prefix+"'");
    
    // Add prefix and transform .md URL
    eleventyConfig.addTransform("htmlmin", function(content, inputPath, outputPath) {
        // replace href="/ with site URL prefix
        content = content.replace(/href=\"\//g,"href=\""+prefix+"/");
        content = content.replace(/src=\"\//g,"src=\""+prefix+"/");
        
        // replace .md files with ./index.html reference
        let regx = /=\"\/(?<url>\S+).md/g;  // hardlink
        content = content.replace(regx, '=\"\/$<url>\/index.html');

        return content;
      });
   
    return {
        passthroughFileCopy: true,
        markdownTemplateEngine: 'njk',
        htmlTemplateEngine: "njk",
        dataTemplateEngine: "njk",
        dir: {
            input: ".",
            output: "_site"
        }
    };
}