---
title: "VMware Reference Architecture"
author: [Pedro Ferreira]
date: 2020-08-25
subject: "VMware"
keywords: [VMware, Public Cloud, Reference Architecture, RA]
subtitle: "Public Cloud Reference Architecture"
classification: "FUJITSU RESTRICTED, COMMERCIAL IN CONFIDENCE"
copyrightyear: "2020"
version: 0.1
lang: "en"
---
# VMware Reference Architecture

